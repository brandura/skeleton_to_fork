// Simple DP Strategy test

// This is the Strategy

public interface IBehaviour {
    public int moveCommand();    
}
