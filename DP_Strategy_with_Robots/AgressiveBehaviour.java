// This is ConcreteStrategy A

public class AgressiveBehaviour implements IBehaviour{
	public int moveCommand() {
		System.out.println("\tAgressive Behaviour: if you find another robot attack it");
		return 1;
	}
}
