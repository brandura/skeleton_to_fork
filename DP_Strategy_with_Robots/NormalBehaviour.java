// This is ConcreteStrategy C

public class NormalBehaviour implements IBehaviour{
	public int moveCommand() {
		System.out.println("\tNormal Behaviour: if you find another robot ignore it");
		return 0;
	}
}
