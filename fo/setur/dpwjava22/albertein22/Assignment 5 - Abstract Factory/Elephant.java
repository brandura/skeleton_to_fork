//------------------------------------------------------------------------------
// File Elephant.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class Elephant implements Obstacle {

	int delay = 2000;
	String type;

	// constructor
	public Elephant(String type) {
		this.type = type;
	}
	
	public boolean tryToPass(Player player, Action pAct) {
		if (!pAct.doIt(player))
			return false;
		System.out.print("--> ");
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (pAct.typeStr.equals("CastAspellAction")) {
			System.out.print("You cannot cast a spell on an an elephant.\n");
			return false;
		}
		if (pAct.typeStr.equals("RunAndHideAction")) {
			System.out.print("Pure optimism, you can't outrun an elephant.\n");
			return false;
		}
		if (pAct.typeStr.equals("BargainAndBuyAction")) {
			System.out.print("Elephants don't talk, do they?\n");
			return false;
		}
		if (pAct.typeStr.equals("ChainsawAction")) {
			System.out.print("Oh no! That's too cruel!.\n");
			return false;
		}
		if (pAct.typeStr.equals("BowAndShakeHandsAction")) {
			System.out.print("Shake foots you mean? No, this is ridiculous!\n");
			return false;
		}
		if (pAct.typeStr.equals("ClimbAction")) {
			System.out.print("Congratulations, you are riding the elephant!\n");
			return true;
		}
		System.err.print("Unknown action for Elephant. Terminating.");
		return false;
	}
	
	public void show() {
		System.out.print("\n>> You are now facing an enormous indian elephant.\n");
	}
}
