//------------------------------------------------------------------------------
// File NastyGameFactory.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
//                 code to be completed by student
//------------------------------------------------------------------------------

import java.util.ArrayList;

public class NastyGameFactory implements GameFactory {

	/**
     * NastyGameFactory:
     *
     * Suitable operations
     * (implementations of GameFactory.java)
     *
     * Obstacles:
     * Dragon, Bomb, Samuray, SumoWrestler, Gangster
     *
     * Possible Actions:
     * BowAndShakeHandsAction, RunAndHideAction, SwordAction, RifleAction,
     * OfferFoodAction, ChainsawAction, CastAspellAction, BargainAndBuyAction, SurrenderAction
     *
     * Player:
     * HeroPlayer
     */
}