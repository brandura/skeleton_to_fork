//------------------------------------------------------------------------------
// File Samuray.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class Samuray implements Obstacle {

	int delay = 2000;
	String type;

	// constructor
	public Samuray(String type) {
		this.type = type;
	}

	public boolean tryToPass(Player player, Action pAct) {
		if (!pAct.doIt(player))
			return false;
		System.out.print("--> ");
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (pAct.typeStr.equals("SwordAction")) {
			System.out.print("That was a big mistake. You are very dead.\n");
			player.kill();
			return false;
		}
		if (pAct.typeStr.equals("RifleAction")) {
			System.out.print("Before you have triggered you are missing your hand...\n");
			player.decHealth();
			return false;
		}
		if (pAct.typeStr.equals("RunAndHideAction")) {
			System.out.print("Running will not save you.\n");
			return false;
		}
		if (pAct.typeStr.equals("ChainsawAction")) {
			System.out.print("You shouldn't have. You are utmost dead.\n");
			player.kill();
			return false;
		}
		if (pAct.typeStr.equals("CastAspellAction")) {
			System.out.print("Such Mambo-Jambo doesn't work with a samuray!\n");
			return false;
		}
		if (pAct.typeStr.equals("OfferFoodAction")) {
			System.out.print("Sorry, he has just eaten a big portion of Sushi.\n");
			return false;
		}
		if (pAct.typeStr.equals("BargainAndBuyAction")) {
			System.out.print("Money is of no interest for a real Samuray.\n");
			return false;
		}
		if (pAct.typeStr.equals("BowAndShakeHandsAction")) {
			System.out.print("He bows, take your hands and show you the way.\n");
			return true;
		}
		System.err.print("Unknown action for Samuray. Terminating.");
		return false;
	}
	
	public void show() {
		System.out.print("\n>> You encounter a Samuray swinging his sword...\n");
	}
}
