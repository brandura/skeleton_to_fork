//------------------------------------------------------------------------------
// File SumoWrestler.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class SumoWrestler implements Obstacle {

	int delay = 2000;
	String type;

	// constructor
	public SumoWrestler(String type) {
		this.type = type;
	}

	public boolean tryToPass(Player player, Action pAct) {
		if (!pAct.doIt(player))
			return false;
		System.out.print("--> ");
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (pAct.typeStr.equals("SwordAction")) {
			System.out.print("A slice of pork falls to the ground. Thats all.\n");
			return false;
		}
		if (pAct.typeStr.equals("RifleAction")) {
			System.out.print("The bullet is burried in a thick layer of pork. Thats all.\n");
			return false;
		}
		if (pAct.typeStr.equals("RunAndHideAction")) {
			System.out.print("This will not help you pass.\n");
			return false;
		}
		if (pAct.typeStr.equals("ChainsawAction")) {
			System.out.print("Your chainsaw is not powerful enough.\n");
			return false;
		}
		if (pAct.typeStr.equals("CastAspellAction")) {
			System.out.print("Such Mambo-Jambo doesn't work with a real Sumo!\n");
			return false;
		}
		if (pAct.typeStr.equals("BowAndShakeHandsAction")) {
			System.out.print("He bows... AND ATTACKS YOU. You are flat and hurt.\n");
			player.decHealth();
			return false;
		}
		if (pAct.typeStr.equals("BargainAndBuyAction")) {
			System.out.print("Money is of no interest for a real SUMO.\n");
			return false;
		}
		if (pAct.typeStr.equals("OfferFoodAction")) {
			System.out.print("You pass silently while the Sumo concentrates on eating.\n");
			return true;
		}
		System.out.print("Unknown action for SumoWrestler. Terminating.");
		return false;
	}
	
	public void show() {
		System.out.print("\n>> You encounter a 256.7-kg Sumo wrestler...\n");
	}
}
