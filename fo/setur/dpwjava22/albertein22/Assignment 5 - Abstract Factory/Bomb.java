//------------------------------------------------------------------------------
// File Bomb.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class Bomb implements Obstacle {

	int delay = 2000;
	String type;

	// constructor
	public Bomb(String type) {
		this.type = type;
	}
	
	public boolean tryToPass(Player player, Action pAct) {
		if (!pAct.doIt(player))
			return false;
		System.out.print("--> ");
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (pAct.typeStr.equals("SwordAction")) {
			System.out.print("BOOM!\n");
			player.kill();
			return false;
		}
		if (pAct.typeStr.equals("RifleAction")) {
			System.out.print("KABOOM!\n");
			player.kill();
			return false;
		}
		if (pAct.typeStr.equals("ChainsawAction")) {
			System.out.print("BAAAM!\n");
			player.kill();
			return false;
		}
		if (pAct.typeStr.equals("BowAndShakeHandsAction")) {
			System.out.print("With a bomb? BOOM!\n");
			player.kill();
			return false;
		}
		if (pAct.typeStr.equals("CastAspellAction")) {
			System.out.print("That doesn't help with a bomb.\n");
			return false;
		}
		if (pAct.typeStr.equals("OfferFoodAction")) {
			System.out.print("Bombs don't eat, do they?\n");
			return false;
		}
		if (pAct.typeStr.equals("BargainAndBuyAction")) {
			System.out.print("With a BOMB??\n");
			return false;
		}
		if (pAct.typeStr.equals("RunAndHideAction")) {
			System.out.print("You got away with some injuries. You can continue.\n");
			player.decHealth();
			return true;
		}
		System.err.print("Unknown action for Bomb. Terminating.");
		return false;
	}
	
	public void show() {
		System.out.print("\n>> You stumble over a smoking package labled 'TNT-Explosives'.\n");
	}
}
