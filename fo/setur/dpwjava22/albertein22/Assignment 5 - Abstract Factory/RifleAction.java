//------------------------------------------------------------------------------
// File RifleAction.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class RifleAction extends Action {
	
	// constructor
	public RifleAction() {
		super("RifleAction", "Use your rifle\n");
	}
	
	@Override
	public boolean doIt(Player player) {
		if (player.getAmmo() > 0) {
			System.out.println("You are shooting with your rifle...");
            player.decAmmo();
		}
        else {
        	System.out.println("Sorry. You're out of ammunition.");
            return false;
        }
        return true;
    }
}
