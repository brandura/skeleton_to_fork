//------------------------------------------------------------------------------
// File SurrenderAction.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class SurrenderAction extends Action {
	
	// constructor
	public SurrenderAction() {
		super("SurrenderAction", "I surrender!\n");
	}
	
	@Override
	public boolean doIt(Player player) {
		player.kill();
        return false;
    }
}
