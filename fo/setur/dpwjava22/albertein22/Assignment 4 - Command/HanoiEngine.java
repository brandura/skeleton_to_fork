package fo.setur.dpwjava22.albertein22.assignment4;
// class HanoiEngine.java
// Course: Design Patterns with Java (5041.22)
// Game Engine file for Assignment 4 - Command
// Hilmar Simonsen, 3/2-2022

import java.util.ArrayList;

public class HanoiEngine {

	int iDiscs;
	int iMoves = 0;
	// Declare a nested ArrayList object
	ArrayList<ArrayList<Integer>> iTowers;

	// constructor
	public HanoiEngine(int aDiscs) {
		if(aDiscs == 0) {
			aDiscs = 5;  // default number of discs
		}
		iDiscs = aDiscs;
		
		// First instantiate the outer ArrayList
		iTowers = new ArrayList<ArrayList<Integer>>();
		
		// Then instantiate the 3 inner (nested) ArrayLists (the towers)
		for (int i = 0; i < 3; i++) {
			iTowers.add(new ArrayList<Integer>());
		}
		// Lastly, populate the 1st tower with iDiscs number of discs
		for (int i = iDiscs; i > 0; --i) {
			iTowers.get(0).add(0, i);  // insert to front
		}
	}

	// Display the towers with the discs
	public void show() {
		System.out.println();
		for (int row = 0; row < iDiscs; ++row) {
			for (int tower = 0; tower < 3; ++tower) {
				if ((iDiscs - row) > iTowers.get(tower).size()) {
					System.out.print("\t.");
				} else {
					int index = row - iDiscs + iTowers.get(tower).size();
					System.out.print("\t" + iTowers.get(tower).get(index));
				}
			}
			System.out.println();
		}
		System.out.println("\t==\t==\t==");
		System.out.println("\tT1\tT2\tT3\tmoves: " + iMoves);
		if (isDone()) {
			System.out.println("\nCongratulations - you did it!");
		}
	}
	
	// Move a disc from tower aFrom to tower aTo.
	// A successful move returns true
	public boolean move(int aFrom, int aTo) {
		// Is this move legal?
		if (iTowers.get(aFrom - 1).size() != 0 && (iTowers.get(aTo - 1).size() == 0 || 
				iTowers.get(aFrom - 1).get(0) < iTowers.get(aTo - 1).get(0))) {
			// Yes!
			iTowers.get(aTo - 1).add(0, iTowers.get(aFrom - 1).get(0));
			iTowers.get(aFrom - 1).remove(0);
			++iMoves;
			return true;
		}
		// No!
		return false;
	}
	
	// Reset the game, start with aDiscs discs
	public void reset(int aDiscs) {
		for (int i = 0; i < 3; ++i) {
			iTowers.get(i).clear();
		}
		iDiscs = aDiscs;
		iMoves = 0;
		for (int i = iDiscs; i > 0; --i) {
			iTowers.get(0).add(0, i);  // insert to front
		}
	}
	
	public boolean isDone() {
	    // All discs at the second tower?
		return iTowers.get(1).size() == iDiscs;
	}
}
